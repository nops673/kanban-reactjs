import React from 'react';
import './Tarefa.css';
import {useSelector,useDispatch} from 'react-redux';
import TarefaService from '../../../../services/TarefaService';


function Tarefa(props) {
    const listaTarefas = useSelector(state => state.listaTarefas);
    const listaDeColunas = useSelector(state => state.listaDeColunas);
    const dispacth = useDispatch();


    function moverTarefa(tarefa,action) {
        const DONE_BLOCK_CODE = 3;

        switch (action) {
            case 'DELETAR':
                TarefaService ({method:"DELETE", data:{id: tarefa.id}, action, tarefa, disparar});
                break;
            case 'MOVER_PARA_FRENTE':
                tarefa.blockID = tarefa.blockID + 1;
                tarefa.block = listaDeColunas.filter((coluna)=>coluna.id === tarefa.blockID)[0].blockName;

                if(tarefa.blockID === (DONE_BLOCK_CODE)){
                    tarefa.endDate = new Date().toISOString();
                }
                TarefaService ({method:"PUT", data:tarefa, action, tarefa, disparar});
                break;
            case 'MOVER_PARA_TRAS':
                tarefa.blockID = tarefa.blockID - 1;
                tarefa.block = listaDeColunas.filter((coluna)=>coluna.id === tarefa.blockID)[0].blockName;
                TarefaService ({method:"PUT", data:tarefa, action, tarefa, disparar});
                break;
            default:
                //TarefaService ({method:"PUT", data:tarefa});
                break;
        }
        function disparar(action, tarefa) {
            dispacth(
                {
                    type: action,
                    tarefa: tarefa
                }
            )
        }
	}

    return(
        listaTarefas.filter((tarefa)=>{return tarefa.block === props.block.blockName})
        .map(tarefa=>{
            tarefa.blockID = props.block.id;
            return(
                <li className="Tarefa" key={tarefa.id}>
                    <h4>{tarefa.name}</h4>
                    <p>{tarefa.description}</p>

                    {tarefa.blockID > 1 &&
                            
                    <i onClick={()=>{moverTarefa(tarefa, 'MOVER_PARA_TRAS')}} title="Mover para coluna anterior">
                        <svg width="17" height="24" viewBox="0 0 17 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.939341 10.9393C0.353554 11.5251 0.353554 12.4749 0.939341 13.0607L10.4853 22.6066C11.0711 23.1924 12.0208 23.1924 12.6066 22.6066C13.1924 22.0208 13.1924 21.0711 12.6066 20.4853L4.12132 12L12.6066 3.51472C13.1924 2.92893 13.1924 1.97919 12.6066 1.3934C12.0208 0.807611 11.0711 0.807611 10.4853 1.3934L0.939341 10.9393ZM17 10.5L2 10.5V13.5L17 13.5V10.5Z" fill="#008000"/>
                        </svg>
                    </i>
                    }
                            
                    {tarefa.blockID < listaDeColunas.length &&
                        <i onClick={()=>{moverTarefa(tarefa, 'MOVER_PARA_FRENTE')}} title="Mover para proxima coluna">
                            <svg width="17" height="24" viewBox="0 0 17 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M16.0607 13.0607C16.6464 12.4749 16.6464 11.5251 16.0607 10.9393L6.51472 1.3934C5.92893 0.807611 4.97919 0.807611 4.3934 1.3934C3.80761 1.97919 3.80761 2.92893 4.3934 3.51472L12.8787 12L4.3934 20.4853C3.80761 21.0711 3.80761 22.0208 4.3934 22.6066C4.97919 23.1924 5.92893 23.1924 6.51472 22.6066L16.0607 13.0607ZM0 13.5H15V10.5H0V13.5Z" fill="#008000"/>
                            </svg>
                        </i>
                    }
                    <i onClick={()=>{moverTarefa(tarefa, 'DELETAR')}} title="Excluir">
                        <svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <line y1="-1" x2="28.2843" y2="-1" transform="matrix(0.707107 -0.707106 0.707107 0.707106 2 22)" stroke="#FF0000" strokeWidth="2"/>
                            <line y1="-1" x2="28.2843" y2="-1" transform="matrix(0.707107 0.707106 -0.707107 0.707106 2 2)" stroke="#FF0000" strokeWidth="2"/>
                        </svg>
                    </i>
                </li>
            )
        })
    )
}

export default Tarefa;