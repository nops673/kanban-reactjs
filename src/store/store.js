import { createStore } from 'redux';


const INITIAL_STATE = {
    listaTarefas: [
    ],
    listaDeColunas:[
        {
            id: 1,
            name: "To Do",
            blockName: "TODO",
        },
        {
            id: 2,
            name: "Doing",
            blockName: "DOING",
        },
        {
            id: 3,
            name: "Done",
            blockName: "DONE",
        },
        {
            id: 4,
            name: "Other",
            blockName: "OTHER",
        },
    ],
}

function atualizarTarefas({state, action}) {
    const listaTarefas =  state.listaTarefas.map((tarefaNoState)=>{
        if(action.tarefa.id === tarefaNoState.id){
            tarefaNoState.block = action.tarefa.block;
        }
        return tarefaNoState;
    })
    return listaTarefas;
}

function Todo(state = INITIAL_STATE, action) {

    switch (action.type) {
        case 'OBTER_PRIMEIRA_LISTA':
            return { ...state, listaTarefas: action.listaTarefas }
        case 'ADICIONAR_TAREFA':
            return { ...state, listaTarefas: [ ...state.listaTarefas, action.tarefa] }
        case 'DELETAR':
            state.listaTarefas = state.listaTarefas.filter((tarefa)=>tarefa.id !== action.tarefa.id);
            return ( { ...state, listaTarefas: state.listaTarefas} );
        default:
            state.listaTarefas = atualizarTarefas({state, action});
            return state;
    }
}


const store = createStore(Todo);
export default store;