Todo - Kanban 



# Instalacao

Para executar esse projeto, utilize `npm install` na pasta raiz.


# Criado com Create React App

Esse projeto foi criado com [Create React App](https://github.com/facebook/create-react-app).

## Scripts disponiveis

Os seguintes scripts estão disponiveis.

### `npm start`

Executa o projeto em ambiente de desenvolvimento:
Acesse [http://localhost:3000](http://localhost:3000) para ver no navegador.

A pagina recarregará a cada alteração.

### `npm test`

Executa testes de interação com API
