import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import TarefaService from '../../services/TarefaService';

import Coluna from './Coluna/Coluna';

import './TodoKanban.css';

function TodoKanban() {

    const listaDeColunas = useSelector(state => state.listaDeColunas);
    const listaTarefas = useSelector(state => state.listaTarefas);
    const dispacth = useDispatch();


    function submeterFormulario(e) {
        e.preventDefault();
        const nomeNovaTarefa = document.getElementById("nomeNovaTarefa").value;
        const descricaoNovaTarefa = document.getElementById("descricaoNovaTarefa").value;

        if((!nomeNovaTarefa) && (!descricaoNovaTarefa)){return false};

        const novaTarefa = {
            "id": listaTarefas.length + 1,
            "name": (nomeNovaTarefa)?nomeNovaTarefa:"Tarefa sem nome",
            "block": "TODO",
            "description": (descricaoNovaTarefa)?descricaoNovaTarefa:"Tarefa sem descrição",
            "creationDate": new Date().toISOString()
        }
        TarefaService ({method:"POST", data:novaTarefa, action:'ADICIONAR_TAREFA' , tarefa:novaTarefa, disparar});
        function disparar(action, tarefa) {
            dispacth(
                {
                    type: action,
                    tarefa: tarefa
                }
            )
        }

        document.getElementById("nomeNovaTarefa").value = '';
        document.getElementById("descricaoNovaTarefa").value = '';
      }
    return(
        <main>
            <section className="NewTask">
                <form onSubmit={submeterFormulario}>
                    <label htmlFor="newTaskInput">New Task</label>
                    <input type="text" name="nomeNovaTarefa" id="nomeNovaTarefa" placeholder="Task name" autoComplete="off"/>
                    <input type="text" name="descricaoNovaTarefa" id="descricaoNovaTarefa" placeholder="Task description" autoComplete="off"/>
                    <button type="submit">Create</button>
                </form>

            </section>
            <section className="Colunas"> 
                {
                    listaDeColunas.map((coluna) =>{
                        return <Coluna key={coluna.id}  block={coluna}></Coluna>
                    })
                }
            </section>
        </main>
    );
}

export default TodoKanban;