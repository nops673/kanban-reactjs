import Tarefa from './Tarefa/Tarefa';

import './Coluna.css';

function Coluna(props) {

    return(
        <div className="Coluna">
            <h3>{props.block.name}</h3>
            <ul>
                <Tarefa block={props.block}></Tarefa>
            </ul>
        </div>
    );
}

export default Coluna; 