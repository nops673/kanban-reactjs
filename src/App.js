import React, { useEffect } from 'react';
import { Provider, useDispatch} from 'react-redux';
import store from './store/store';

import TarefaService from './services/TarefaService';

import Cabecalho from './components/Cabecalho/Cabecalho';
import TodoKanban from './components/TodoKanban/TodoKanban';

import './lib/Properties.css';
import './lib/Fonts/Fonts.css';

const AppWrapper = () => {
	return (
		<Provider store={store}> 
			<App />
		</Provider>
	)
}

function App() {
	const dispacth = useDispatch();

	useEffect(()=>{
		TarefaService ({method:"GET", data:'', action:'OBTER_PRIMEIRA_LISTA' , tarefa:null, disparar});
        function disparar(action, listadeTarefas) {
			dispacth(
				{
					type: action,
					listaTarefas: listadeTarefas
				}
			)
        }
	})

	return (
		<div className="App">
			<Cabecalho></Cabecalho>
			<TodoKanban></TodoKanban>
		</div> 
	);
}

export default AppWrapper;
