import AxiosCommon from "./AxiosCommon";
import ErrorHandler from "./ErrorHandler";


function TarefaService ({method, data, action, tarefa, disparar}) {
    AxiosCommon({
        method: method,
        data: data
    })
    .then(res => {
        if(!tarefa){tarefa = res.data};
        disparar(action, tarefa);
    }).catch((error)=>{ErrorHandler(error)})
}
export default TarefaService;