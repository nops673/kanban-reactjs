import TarefaService from '../../../../services/AxiosCommon';
import ErrorHandler from '../../../../services/ErrorHandler';


test('Deletar Tarefa API', () => {
    
    const tarefa =     {
        "id": 1,
        "name": "Criar um kaban",
        "block": "TODO",
        "description": "Eu sou um usuário e quero ver todas as minhas tarefas em três diferentes blocos",
        "creationDate": "2021-06-18T08:40:51.000Z"
    }
    return TarefaService.delete(``, {
        data: {id: tarefa.id}
    }).then(res => {
            expect(res.data).toBe("Deletado com sucesso");
        }, err=>ErrorHandler(err))
});

test('Criar Tarefa API', () => {
    
    const tarefa =     {
        "id": 1,
        "name": "Criar um kaban",
        "block": "TODO",
        "description": "Eu sou um usuário e quero ver todas as minhas tarefas em três diferentes blocos",
        "creationDate": "2021-06-18T08:40:51.000Z"
    }
    return TarefaService.post(``, {
        data: {id: tarefa.id}
    }).then(res => {
            expect(res.data).not.toBeNull();
        }, err=>ErrorHandler(err))
});


test('Atualizar Tarefa API', () => {
    
    const tarefa =     {
        "id": 1,
        "name": "Criar um kaban",
        "block": "TODO",
        "description": "Eu sou um usuário e quero ver todas as minhas tarefas em três diferentes blocos",
        "creationDate": "2021-06-18T08:40:51.000Z"
    }
    return TarefaService.put(``, {
        data: {id: tarefa.id}
    }).then(res => {
            expect(res.data).not.toBeNull();
        }, err=>ErrorHandler(err))
});
