import axios from "axios";


function AxiosCommon() {

    const instance = axios.create({
        baseURL: 'https://private-a133d-actdigitalreact.apiary-mock.com/tasks',
        timeout: 3000,
        responseType: 'json',
        headers: {
            "Content-type": "application/json"
        },
    });

    return instance;
}
export default AxiosCommon();